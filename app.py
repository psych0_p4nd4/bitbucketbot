from flask import Flask
from os import environ
from flask_restful import request
import requests


app = Flask(__name__)


def build_message(data):
    """
    This method collects the data necessary to form the answer from the one received in the arguments json
    :param data: json
    :return: str
    """
    repo = data['repository']
    to_repo = repo['name']
    repo_url = repo['links']['html']['href']

    commit_author = data['push']['changes'][0]['new']['target']['author']['raw']

    commit_url = data['push']['changes'][0]['new']['target']['links']['html']['href']
    commit_message = data['push']['changes'][0]['new']['target']['message']
    commit_date = data['push']['changes'][0]['new']['target']['date']
    message = ''

    message += 'New COMMIT\n'
    message += f'By [{commit_author}]\n\n'
    message += f'To [{to_repo}]({repo_url})\n\n'
    message += f'Message: [{commit_message}]({commit_url})\n\n'
    message += f'Date: {commit_date}\n'

    return message


def send_message(chat_id, text, parse_mode=None):
    """
    Send message to bot chat
    :param chat_id: str
    :param text: str
    :param parse_mode: str
    :return: response
    """
    params = {'chat_id': chat_id, 'text': text}
    token = environ.get('TOKEN')
    api_url = f"https://api.telegram.org/bot{token}/"
    if parse_mode is not None and parse_mode is 'Markdown':
        params.update({'parse_mode': parse_mode})
    method = 'sendMessage'
    return requests.post(api_url + method, params)


@app.route('/webhook', methods=['POST'])
def tracking():
    """
    Method acts as a controller, routing requests
    :return: None
    """
    if request.method == 'POST':
        data = request.get_json()
        msg = build_message(data)
        chat_id = '166175154'
        send_message(chat_id, msg, 'Markdown')
        return "Ok"


@app.route('/')
def hello_world():
    """
    I don’t know why, but let it be
    :return: str
    """
    return 'Bitbucket tracked bot'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=environ.get('PORT'))
